import { Document, Schema, Model, model, Error } from "mongoose";

export interface IDistributor extends Document {
  Id: String;
  Name: String;
  Address: String;
  Contact: String;
}

export const distributorSchema = new Schema({
  Id: {
    type: String,
    required: true,
    unique: true
  },
  Name: String,
  Address: String,
  Contact: String
});

export const Distributor: Model<IDistributor> = model<IDistributor>(
  "Distributor",
  distributorSchema
);
