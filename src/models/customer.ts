import { Document, Schema, Model, model, Error } from "mongoose";

export interface ICustomer extends Document {
  Id: String;
  Name: String;
  Address: String;
  Contact: String;
}

export const customerSchema = new Schema({
  Id: {
    type: String,
    required: true,
    unique: true
  },
  Name: String,
  Address: String,
  Contact: String
});

export const Customer: Model<ICustomer> = model<ICustomer>(
  "Customer",
  customerSchema
);
