import { Document, Schema, Model, model, Error } from "mongoose";
import { isIPv4 } from "net";
import { IInvoiceLine } from "./invoiceLine";

export interface IInvoice extends Document {
  Id: String;
  InvoiceNumber: String;
  PurchaseDate: Date;
  CustomerLocationId: String;
  DistributorLocationId: String;
  // InvoiceLines: IInvoiceLine[];
  Qty: Number;
  Weight: Number;
  UnitOfMeasure: String;
  UnitPrice: Number;
  ProductId: String;
  ManufacturerId: String;
  TotalPrice: Number;
}

export const invoiceSchema = new Schema({
  Id: {
    type: String,
    required: true,
    unique: true
  },
  InvoiceNumber: String,
  PurchaseDate: Date,
  CustomerLocationId: {
    ref: "Customer",
    type: Schema.Types.ObjectId
  },
  DistributorLocationId: {
    ref: "Distributor",
    type: Schema.Types.ObjectId
  },

  Qty: Number,
  Weight: Number,
  UnitOfMeasure: String,
  UnitPrice: Number,
  ProductId: {
    ref: "Producto",
    type: Schema.Types.ObjectId
  },
  ManufacturerId: {
    ref: "Manufacturer",
    type: Schema.Types.ObjectId
  },
  TotalPrice: Number
});

/* export const invoiceSchema = new Schema({
  Id: {
    type: String,
    required: true,
    unique: true
  },
  InvoiceNumber: String,
  PurchaseDate: Date,
  CustomerLocationId: {
    ref: "Customer",
    type: Schema.Types.ObjectId
  },
  DistributorLocationId: {
    ref: "Distributor",
    type: Schema.Types.ObjectId
  },
  InvoiceLines: [
    {
      Qty: Number,
      Weight: Number,
      UnitOfMeasure: String,
      UnitPrice: Number,
      ProductId: {
        ref: "Producto",
        type: Schema.Types.ObjectId
      },
      ManufacturerId: {
        ref: "Manufacturer",
        type: Schema.Types.ObjectId
      },
      TotalPrice: Number
    }
  ]
});*/

export const Invoice: Model<IInvoice> = model<IInvoice>(
  "Invoice",
  invoiceSchema
);
