import { Document, Schema, Model, model, Error } from "mongoose";
import { IProducto } from "./producto";

export interface IInvoiceLine {
  Qty: String;
  Weight: Date;
  UnitOfMeasure: Number;
  UnitPrice: String;
  ProductId: IProducto;
}
