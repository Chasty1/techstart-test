import { Document, Schema, Model, model, Error } from "mongoose";

export interface IManufacturer extends Document {
  Id: String;
  Name: String;
  Address: String;
  Contact: String;
}

export const manufacturerSchema = new Schema({
  Id: {
    type: String,
    required: true,
    unique: true
  },
  Name: String,
  Address: String,
  Contact: String
});

export const Manufacturer: Model<IManufacturer> = model<IManufacturer>(
  "Manufacturer",
  manufacturerSchema
);
