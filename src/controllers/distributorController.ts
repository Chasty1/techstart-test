import { Request, Response } from "express";
import { IDistributor, Distributor } from "../models/distributor";

export class DistributorController {
  public async getDistributors(req: Request, res: Response): Promise<void> {
    const distributors = await Distributor.find();
    res.json(distributors);
  }

  public async getProducto(req: Request, res: Response): Promise<void> {
    const distributor = await Distributor.findOne({ Id: req.params.id });
    if (distributor === null) {
      res.sendStatus(404);
    } else {
      res.json(distributor);
    }
  }

  public async create(req: Request, res: Response): Promise<void> {
    const newDistributor: IDistributor = new Distributor(req.body);
    const distributor = await Distributor.findOne({ Id: req.body.Id });
    if (distributor === null) {
      const result = await newDistributor.save();
      if (result === null) {
        res.sendStatus(500);
      } else {
        res.status(201).json({ status: 201, data: result });
      }
    } else {
      res.sendStatus(422);
    }
  }
}
