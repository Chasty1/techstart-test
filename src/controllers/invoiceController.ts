import { Request, Response } from "express";
import { IInvoice, Invoice } from "../models/invoice";

export class InvoiceController {
  public async getInvoices(req: Request, res: Response): Promise<void> {
    const invoices = await Invoice.find()
      .populate({
        path: "CustomerLocationId"
      })
      .populate({
        path: "DistributorLocationId"
      })
      .populate({
        path: "ManufacturerId"
      })
      .populate({
        path: "ProductId"
      });
    res.json(invoices);
  }

  public async getDifferentInvoices(
    req: Request,
    res: Response
  ): Promise<void> {
    const invoices = await Invoice.aggregate([
      {
        $group: {
          _id: "$InvoiceNumber",
          docs: {
            $first: {
              Id: "$_id",
              PurchaseDate: "$PurchaseDate"
            }
          }
        }
      }
    ]);

    res.json(invoices);
  }

  public async getInvoicesByProduct(
    req: Request,
    res: Response
  ): Promise<void> {
    // ProductCode: 123
    console.log(req.body);

    try {
      const invoices = await Invoice.find({
        ProductId: req.body.ProductId
      })
        .populate({
          path: "CustomerLocationId"
        })
        .populate({
          path: "DistributorLocationId"
        })
        .populate({
          path: "ManufacturerId"
        })
        .populate({
          path: "ProductId"
        });
      res.json(invoices);
    } catch (e) {
      res.json([]);
    }
  }

  public async getInvoicesByDistributor(
    req: Request,
    res: Response
  ): Promise<void> {
    // ProductCode: 123
    console.log(req.body);

    try {
      const invoices = await Invoice.find({
        DistributorLocationId: req.body.DistributorId
      })
        .populate({
          path: "CustomerLocationId"
        })
        .populate({
          path: "DistributorLocationId"
        })
        .populate({
          path: "ManufacturerId"
        })
        .populate({
          path: "ProductId"
        });
      res.json(invoices);
    } catch (e) {
      res.json([]);
    }
  }

  public async getInvoicesByCustomerLocation(
    req: Request,
    res: Response
  ): Promise<void> {
    // ProductCode: 123
    console.log(req.body);

    try {
      const invoices = await Invoice.find({
        CustomerLocationId: req.body.CustomerId
      })
        .populate({
          path: "CustomerLocationId"
        })
        .populate({
          path: "DistributorLocationId"
        })
        .populate({
          path: "ManufacturerId"
        })
        .populate({
          path: "ProductId"
        });
      res.json(invoices);
    } catch (e) {
      res.json([]);
    }
  }

  public async getInvoicesByInvoice(
    req: Request,
    res: Response
  ): Promise<void> {
    // ProductCode: 123
    console.log(req.body);

    try {
      const invoices = await Invoice.find({
        InvoiceNumber: req.body.InvoiceNumber
      })
        .populate({
          path: "CustomerLocationId"
        })
        .populate({
          path: "DistributorLocationId"
        })
        .populate({
          path: "ManufacturerId"
        })
        .populate({
          path: "ProductId"
        });
      res.json(invoices);
    } catch (e) {
      res.json([]);
    }
  }

  public async getInvoice(req: Request, res: Response): Promise<void> {
    const product = await Invoice.findOne({ productId: req.params.id });
    if (product === null) {
      res.sendStatus(404);
    } else {
      res.json(product);
    }
  }

  public async createInvoice(req: Request, res: Response): Promise<void> {
    const newProduct: IInvoice = new Invoice(req.body);
    console.log(req.body);
    const product = await Invoice.findOne({ productId: req.body.Id });
    if (product === null) {
      const result = await newProduct.save();
      if (result === null) {
        res.sendStatus(500);
      } else {
        res.status(201).json({ status: 201, data: result });
      }
    } else {
      res.sendStatus(422);
    }
  }

  public async updateInvoice(req: Request, res: Response): Promise<void> {
    const product = await Invoice.findOneAndUpdate(
      { productId: req.params.id },
      req.body
    );
    if (product === null) {
      res.sendStatus(404);
    } else {
      const updatedProduct = { productId: req.params.id, ...req.body };
      res.json({ status: res.status, data: updatedProduct });
    }
  }

  public async deleteInvoice(req: Request, res: Response): Promise<void> {
    const product = await Invoice.findOneAndDelete({
      productId: req.params.id
    });
    if (product === null) {
      res.sendStatus(404);
    } else {
      res.json({ response: "Invoice deleted Successfully" });
    }
  }
}
