import { Request, Response } from "express";
import { IProducto, Producto } from "../models/producto";

export class ProductoController {
  public async getProductos(req: Request, res: Response): Promise<void> {
    const productos = await Producto.find();
    res.json(productos);
  }

  public async getProducto(req: Request, res: Response): Promise<void> {
    const product = await Producto.findOne({ Id: req.params.id });
    if (product === null) {
      res.sendStatus(404);
    } else {
      res.json(product);
    }
  }

  public async create(req: Request, res: Response): Promise<void> {
    const newProduct: IProducto = new Producto(req.body);
    const product = await Producto.findOne({ Id: req.body.Id });
    if (product === null) {
      const result = await newProduct.save();
      if (result === null) {
        res.sendStatus(500);
      } else {
        res.status(201).json({ status: 201, data: result });
      }
    } else {
      res.sendStatus(422);
    }
  }
}
