import { Request, Response } from "express";
import { ICustomer, Customer } from "../models/customer";

export class CustomerController {
  public async getCustomers(req: Request, res: Response): Promise<void> {
    const customers = await Customer.find();
    res.json(customers);
  }

  public async getProducto(req: Request, res: Response): Promise<void> {
    const customer = await Customer.findOne({ Id: req.params.id });
    if (customer === null) {
      res.sendStatus(404);
    } else {
      res.json(customer);
    }
  }

  public async create(req: Request, res: Response): Promise<void> {
    const newCustomer: ICustomer = new Customer(req.body);
    const customer = await Customer.findOne({ Id: req.body.Id });
    if (customer === null) {
      const result = await newCustomer.save();
      if (result === null) {
        res.sendStatus(500);
      } else {
        res.status(201).json({ status: 201, data: result });
      }
    } else {
      res.sendStatus(422);
    }
  }
}
