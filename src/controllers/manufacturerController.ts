import { Request, Response } from "express";
import { IManufacturer, Manufacturer } from "../models/manufacturer";

export class ManufacturerController {
  public async getManufacturers(req: Request, res: Response): Promise<void> {
    const manufacturers = await Manufacturer.find();
    res.json(manufacturers);
  }

  public async getManufacturer(req: Request, res: Response): Promise<void> {
    const manufacturer = await Manufacturer.findOne({ Id: req.params.id });
    if (manufacturer === null) {
      res.sendStatus(404);
    } else {
      res.json(manufacturer);
    }
  }

  public async createManufacturer(req: Request, res: Response): Promise<void> {
    const newManufacturer: IManufacturer = new Manufacturer(req.body);
    const manufacturer = await Manufacturer.findOne({ Id: req.body.Id });
    if (manufacturer === null) {
      const result = await newManufacturer.save();
      if (result === null) {
        res.sendStatus(500);
      } else {
        res.status(201).json({ status: 201, data: result });
      }
    } else {
      res.sendStatus(422);
    }
  }
}
