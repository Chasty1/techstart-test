const subscribersMock = {
    customer_id: 987654,
    subscribers:
        [
            {
                id: 1155446677,
                product_type: "mobile",
                offer_id: 123,
                status: "actived",
                id_fa: "FA_550987654"
            },
            {
                id: 1155557777,
                product_type: "mobile",
                offer_id: 123,
                status: "suspended",
                id_fa: "FA_550987654"
            },
            {
                id: 1199887766,
                product_type: "broadband",
                offer_id: 215,
                status: "actived",
                id_fa: "FA_55678910"
            },
            {
                id: 1140401234,
                product_type: "fixed phone",
                offer_id: 300,
                status: "actived",
                id_fa: "FA_55678910"
            }
        ]
};

export default subscribersMock;