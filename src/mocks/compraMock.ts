const compraMock = {
    "items": [
        {
            "id": 123,
            "name": "Plan Comunidad 10GB ",
            "product_type": "mobile",
            "price": 2000,
            "type": "oferta"
        },
        {
            "id": 125,
            "name": "Plan Comunidad 20GB ",
            "product_type": "mobile",
            "price": 2500,
            "type": "oferta"
        },
        {
            "id": 210,
            "name": "Banda Ancha 100MB",
            "product_type": "broadband",
            "price": 1500,
            "type": "oferta"
        },
        {
            "id": 215,
            "name": "Banda Ancha 300MB Fibra",
            "product_type": "broadband",
            "price": 2500,
            "type": "oferta"
        },
        {
            "id": 300,
            "name": "Linea Hogar",
            "product_type": "fixed phone",
            "price": 250,
            "type": "oferta"
        },
        {
            "id": 400,
            "name": "TV Full HD",
            "product_type": "tv",
            "price": 2000,
            "type": "oferta"
        },
    ]
};

export default compraMock;