const representantesMock = [
    {
        id: 1234,
        name: "Ana Nodes",
        rol: "Retencion"
    },
    {
        id: 5678,
        name: "Juan Reacto",
        rol: "Callcenter"
    }
];

export default representantesMock;