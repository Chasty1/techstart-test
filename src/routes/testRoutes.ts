import { Router } from "express";
import { TestController } from "../controllers/testController";

export class TestRoutes {
  public router: Router;
  public testController: TestController = new TestController();

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/", this.testController.getTests);
  }
}
