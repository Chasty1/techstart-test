import { Router } from "express";
import { AuthController } from "../controllers/authController";
import { RepresentantesController } from "../controllers/getRepresentantes/controller";
import { GetComprasController } from "../controllers/getCompra/controller";
import { GetCuentaCorrienteController } from "../controllers/getCuentaCorriente/controller";
import { GetCustomersController } from "../controllers/getCustomers/controller";
import { GetSubscribersController } from "../controllers/getSubscribers/controller";

export class ApiRoutes {

    public router: Router;
    public authController: AuthController = new AuthController();
    public representantesController: RepresentantesController = new RepresentantesController();
    public getComprasController: GetComprasController = new GetComprasController();
    public getCuentaCorrienteController: GetCuentaCorrienteController = new GetCuentaCorrienteController();
    public getCustomersController: GetCustomersController = new GetCustomersController();
    public getSubscribersController: GetSubscribersController = new GetSubscribersController();

    constructor() {
        this.router = Router();
        this.routes();
    }
    routes() {
        /*        */
        this.router.get("/api.rodar/reps/:id", this.authController.authenticateJWT, this.representantesController.getRepresentantes);
        /*        */
        this.router.get("/api.rodar/replace_offer", this.authController.authenticateJWT, this.getComprasController.getCompras);
        /*        */
        this.router.get("/api.rodar/finantial_accounts/:customer_id", this.authController.authenticateJWT, this.getCuentaCorrienteController.getCuentaCorriente);
        /*        */
        this.router.get("/api.rodar/customers/:id", this.authController.authenticateJWT, this.getCustomersController.getCustomers);
        /*        */
        this.router.get("/api.rodar/subscribers/:customer_id", this.authController.authenticateJWT, this.getSubscribersController.getSubscribers);
        /*        */
        this.router.post("/api.rodar/replace_offer", this.authController.authenticateJWT, this.getComprasController.postCompras);
    }
}