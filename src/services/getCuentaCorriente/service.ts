import { logger } from "../../configuration/log4js";
import { Request } from "express";
import cuentaCorrienteMock from "../../mocks/cuentaCorrienteMock";

const SOSAmountsUrl = "SOSURLMOCK";

function getCuentaCorrienteById(id: number) {
    if (id === cuentaCorrienteMock.customer_id) {
        return cuentaCorrienteMock;
    }
}

const getCuentaCorriente = async (req: Request) => {
    try {
        const data = getCuentaCorrienteById(parseInt(req.params.customer_id));

        logger.info(`\nMETHOD: GET\nDATA: ${JSON.stringify(data)}`);

        return data;
    } catch (e) {
        logger.error(`\nURL: ${SOSAmountsUrl}\nMETHOD: GET\nERROR: ${e.message} - ${e.response.data.message}`);
        return {
            data: {
                URL: SOSAmountsUrl,
                METHOD: "GET",
                ERROR: `${e.message} - ${e.response.data.message}`,
            },
            status: e.response.status,
        };
    }
};

export default { getCuentaCorriente };