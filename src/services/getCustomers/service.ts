import { logger } from "../../configuration/log4js";
import { Request } from "express";
import customersMock from "../../mocks/customersMock";

const SOSAmountsUrl = "SOSURLMOCK";
function filterCustomers(id: number) {
  return customersMock.filter((singleCustomer) => singleCustomer.id === id);
}

const getCustomers = async (req: Request) => {
  try {
    const data = filterCustomers(parseInt(req.params.id));

    logger.info(`\nMETHOD: GET\nDATA: ${JSON.stringify(data)}`);

    return data;
  } catch (e) {
    logger.error(
      `\nURL: ${SOSAmountsUrl}\nMETHOD: GET\nERROR: ${e.message} - ${e.response.data.message}`
    );
    return {
      data: {
        URL: SOSAmountsUrl,
        METHOD: "GET",
        ERROR: `${e.message} - ${e.response.data.message}`,
      },
      status: e.response.status,
    };
  }
};

export default { getCustomers };
